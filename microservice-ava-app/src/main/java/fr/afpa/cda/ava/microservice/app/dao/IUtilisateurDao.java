package fr.afpa.cda.ava.microservice.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda.ava.microservice.app.dao.entities.Utilisateur;

@Repository
public interface IUtilisateurDao extends JpaRepository<Utilisateur, Integer> {

    // recherche par nom de methode
    Utilisateur findByEmailAndPassword(String login, String password);

    Utilisateur findByNomOrPrenom(String nom, String prenom);

}
