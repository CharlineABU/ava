package fr.afpa.cda.ava.microservice.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceAvaAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceAvaAppApplication.class, args);
	}

}
