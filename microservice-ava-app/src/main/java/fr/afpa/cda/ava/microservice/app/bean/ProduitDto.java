/**
 * 
 */
package fr.afpa.cda.ava.microservice.app.bean;

import java.nio.file.Paths;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.afpa.cda.ava.microservice.app.controller.FileUploadController;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Classe ProduitDto
 * 
 * @author Charline
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProduitDto {

    private Integer id;
    private String  reference;
    @JsonAlias("lib")
    private String  libelle;
    private Double  prix;
    @JsonIgnore
    private String  pathImage;
    private String  url;
    private String  prestataire;

    public String getUrl() {
        if (pathImage == null || id == null)
            return null;
        this.url = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveImage", this.id, Paths.get(this.pathImage).getFileName().toString()).build().toString();

        return this.url;
    }

}
