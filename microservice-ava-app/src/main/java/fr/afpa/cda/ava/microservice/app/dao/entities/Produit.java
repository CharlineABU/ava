/**
 * 
 */
package fr.afpa.cda.ava.microservice.app.dao.entities;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Classe Produit
 * 
 * @author Charline
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SequenceGenerator(name = "produit_id_seq", initialValue = 1, allocationSize = 1)
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produit_id_seq")
    private Integer id;
    private String  reference;
    private String  libelle;
    private Double  prix;
    private String  pathImage;
    //nom du prestataire
    private String  prestataire;

}
