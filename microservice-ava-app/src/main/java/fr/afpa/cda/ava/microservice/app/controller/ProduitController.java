package fr.afpa.cda.ava.microservice.app.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.microservice.app.bean.ProduitDto;
import fr.afpa.cda.ava.microservice.app.services.IFileSystemStorageService;
import fr.afpa.cda.ava.microservice.app.services.IProduitService;

@RestController
public class ProduitController {

    @Autowired
    IProduitService           produitService;

    @Autowired
    IFileSystemStorageService fileSystemStorageService;

    // Get http://localhost:9091/produits => findAll
    @GetMapping({ "/produits" })
    public List<ProduitDto> getAll() {
        List<ProduitDto> produits = produitService.findAll();
        return produits;
    }

    // DELETE http://localhost:9091/produits/delete/1 => delete
    @DeleteMapping("/produits/{id}")
    public void supprimerProduit(@PathVariable Integer id) {
        produitService.deleteById(id);
        System.out.println("dgdsfg");
    }

    // GET http://localhost:9091/produits/{id} ==> getById
    @GetMapping("/produits/{id}")
    public ProduitDto chercherProduit(@PathVariable Integer id, HttpServletRequest request) {
        System.out.println(request.getHeader("cle"));
        return produitService.findById(id);
    }

    // POST http://localhost:9091/produits/new => create
    @PostMapping("/produits/new")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ProduitDto save(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,@RequestBody ProduitDto produit) {
        if (uneImage != null && !uneImage.isEmpty())
            return produitService.save(produit, uneImage);
        return produitService.save(produit);
    }

    // PUT http://localhost:9091/produits/{id}
    @PutMapping("/produits/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ProduitDto update(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,@RequestBody ProduitDto produit) {
        if (uneImage != null && !uneImage.isEmpty())
            return produitService.update(produit, uneImage);
        return produitService.update(produit);
    }

}
