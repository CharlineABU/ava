package fr.afpa.cda.ava.microservice.app.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.microservice.app.bean.UtilisateurDto;

public interface IUtilisateurService {
	public List<UtilisateurDto> findAll();

	public UtilisateurDto findById(Integer id);

	public void deleteById(Integer id);

	public UtilisateurDto findByEmailAndPassword(String login, String password);

	/**
	 * metre à jour un utilisateur sans sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public UtilisateurDto update(UtilisateurDto utilisateur);

	/**
	 * metre à jour un utilisateur avec sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public UtilisateurDto update(UtilisateurDto utilisateur, MultipartFile uneImage);

	/**
	 * creation un utilisateur sans sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public UtilisateurDto save(UtilisateurDto utilisateurDto);

	/**
	 * creation un utilisateur avec sauvgarder son image
	 * 
	 * @param utilisateur
	 * @param uneImage
	 * @return utilisateur
	 */
	public UtilisateurDto save(UtilisateurDto utilisateurDto, MultipartFile uneImage);
}
