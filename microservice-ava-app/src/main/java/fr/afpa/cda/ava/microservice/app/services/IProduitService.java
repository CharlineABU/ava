package fr.afpa.cda.ava.microservice.app.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.microservice.app.bean.ProduitDto;

public interface IProduitService {

	public ProduitDto findById(Integer id);

	public List<ProduitDto> findAll();

	public void deleteById(Integer id);

	/**
	 * metre à jour un produit sans sauvgarder son image
	 * 
	 * @param produit
	 * @param uneImage
	 * @return produit
	 */
	public ProduitDto update(ProduitDto produit);

	/**
	 * metre à jour un produit avec sauvgarder son image
	 * 
	 * @param produit
	 * @param uneImage
	 * @return produit
	 */
	public ProduitDto update(ProduitDto produit, MultipartFile uneImage);

	/**
	 * creation un produit sans sauvgarder son image
	 * 
	 * @param produit
	 * @param uneImage
	 * @return produit
	 */
	public ProduitDto save(ProduitDto produitDto);

	/**
	 * creation un produit avec sauvgarder son image
	 * 
	 * @param produit
	 * @param uneImage
	 * @return produit
	 */
	public ProduitDto save(ProduitDto produitDto, MultipartFile uneImage);

}
