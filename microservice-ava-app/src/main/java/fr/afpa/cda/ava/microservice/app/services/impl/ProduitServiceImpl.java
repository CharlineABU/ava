package fr.afpa.cda.ava.microservice.app.services.impl;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.microservice.app.bean.ProduitDto;
import fr.afpa.cda.ava.microservice.app.dao.IProduitDao;
import fr.afpa.cda.ava.microservice.app.dao.entities.Produit;
import fr.afpa.cda.ava.microservice.app.properties.StorageProperties;
import fr.afpa.cda.ava.microservice.app.services.IFileSystemStorageService;
import fr.afpa.cda.ava.microservice.app.services.IProduitService;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ProduitServiceImpl implements IProduitService {

    @Autowired
    IProduitDao               produitDao;
    @Autowired
    private ModelMapper       modelMapper;
    @Autowired
    IFileSystemStorageService fileSystemStorageService;
    @Autowired
    StorageProperties         storageProperties;

    @Override
    public ProduitDto findById(Integer id) {
        Optional<Produit> o = produitDao.findById(id);
        if (!o.isPresent()) {
            return null;
        }
        Produit resultatDO = o.get();
        return modelMapper.map(resultatDO, ProduitDto.class);
    }

    @Override
    public List<ProduitDto> findAll() {
        return produitDao.findAll().stream().map(entity -> modelMapper.map(entity, ProduitDto.class)).collect(Collectors.toList());

    }

    @Override
    public void deleteById(Integer id) {

        if (findById(id) != null)
            produitDao.deleteById(id);

    }

    /**
     *
     */
    @Override
    public ProduitDto save(ProduitDto newProduit) {

        Produit newProduitDO = modelMapper.map(newProduit, Produit.class);
        newProduitDO = produitDao.save(newProduitDO);
        ProduitDto savedProduitDto = modelMapper.map(newProduitDO, ProduitDto.class);
        return savedProduitDto;
    }

    /**
     *
     */
    @Override
    public ProduitDto save(ProduitDto produitDto, MultipartFile uneImage) {

        Produit produitDoResult = produitDao.save(modelMapper.map(produitDto, Produit.class));
        String chemin = MessageFormat.format(storageProperties.getProduitImageDir(), produitDoResult.getId());
        Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
        produitDoResult.setPathImage(path.toString());
        produitDoResult = produitDao.save(produitDoResult);
        return modelMapper.map(produitDoResult, ProduitDto.class);
    }

    /**
     *
     */
    @Override
    public ProduitDto update(ProduitDto newProduitDto) {
        // remettre le chemin du produit dans le nouveau produit
        Produit oldProduitDo = produitDao.findById(newProduitDto.getId()).get();
        Produit newProduitDO = modelMapper.map(newProduitDto, Produit.class);
        newProduitDO.setPathImage(oldProduitDo.getPathImage());
        newProduitDO = produitDao.save(newProduitDO);
        ProduitDto savedProduitDto = modelMapper.map(newProduitDO, ProduitDto.class);
        return savedProduitDto;
    }

    /**
     *
     */
    @Override
    public ProduitDto update(ProduitDto produitDto, MultipartFile uneImage) {

        Produit produitDoResult = modelMapper.map(produitDto, Produit.class);

        String chemin = MessageFormat.format(storageProperties.getProduitImageDir(), produitDoResult.getId());
        Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
        produitDoResult.setPathImage(path.toString());
        produitDoResult = produitDao.save(produitDoResult);
        return modelMapper.map(produitDoResult, ProduitDto.class);
    }
}
