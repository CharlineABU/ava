package fr.afpa.cda.ava.microservice.app.dao.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SequenceGenerator(name = "utilisateur_id_seq", initialValue = 1, allocationSize = 1)
public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_id_seq")
	@Column(name = "iduser")
	private Integer id;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	@Temporal(TemporalType.DATE)
	private Date dateNaissance;
	private String profil;
	private String pathImage;
	private String url;
	private String adresse;
	private String complementAdresse;
	private String codePostal;
	private String ville;
	private String pays;
	private String fix;
	private String portable;
}
