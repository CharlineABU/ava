package fr.afpa.cda.ava.microservice.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.cda.ava.microservice.app.dao.entities.Produit;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface IProduitDao extends JpaRepository<Produit, Integer> {
    //1-CRUD
}
