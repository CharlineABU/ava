package fr.afpa.cda.ava.microservice.app.services.impl;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.microservice.app.bean.UtilisateurDto;
import fr.afpa.cda.ava.microservice.app.dao.IUtilisateurDao;
import fr.afpa.cda.ava.microservice.app.dao.entities.Utilisateur;
import fr.afpa.cda.ava.microservice.app.properties.StorageProperties;
import fr.afpa.cda.ava.microservice.app.services.IFileSystemStorageService;
import fr.afpa.cda.ava.microservice.app.services.IUtilisateurService;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService {

    @Autowired
    IUtilisateurDao           utilisateurDao;
    @Autowired
    private ModelMapper       modelMapper;
    @Autowired
    IFileSystemStorageService fileSystemStorageService;
    @Autowired
    StorageProperties         storageProperties;

    @Override
    public List<UtilisateurDto> findAll() {

        return utilisateurDao.findAll().stream().map(entity -> modelMapper.map(entity, UtilisateurDto.class)).collect(Collectors.toList());
    }

    @Override
    public UtilisateurDto findById(Integer id) {
        return modelMapper.map(utilisateurDao.getOne(id), UtilisateurDto.class);
    }

    @Override
    public void deleteById(Integer id) {
        utilisateurDao.deleteById(id);

    }

    @Override
    public UtilisateurDto findByEmailAndPassword(String login, String password) {
        UtilisateurDto resultatDoToDto = modelMapper.map(utilisateurDao.findByEmailAndPassword(login, password), UtilisateurDto.class);

        return resultatDoToDto;

    }

    @Override
    public UtilisateurDto save(UtilisateurDto nouveauUtilisateur) {

        Utilisateur paramToDo = modelMapper.map(nouveauUtilisateur, Utilisateur.class);
        paramToDo = utilisateurDao.save(paramToDo);
        return modelMapper.map(paramToDo, UtilisateurDto.class);
    }

    @Override
    public UtilisateurDto save(UtilisateurDto utilisateurDto, MultipartFile uneImage) {
        Utilisateur utilisateurDoResult = utilisateurDao.save(modelMapper.map(utilisateurDto, Utilisateur.class));
        String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), utilisateurDoResult.getId());
        Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
        utilisateurDoResult.setPathImage(path.toString());
        utilisateurDoResult = utilisateurDao.save(utilisateurDoResult);
        return modelMapper.map(utilisateurDoResult, UtilisateurDto.class);
    }

    @Override
    public UtilisateurDto update(UtilisateurDto nouveauUtilisateur) {
        // chercher l'ancien utilisateur en BDD
        Utilisateur oldUtilisateurDo = utilisateurDao.findById(nouveauUtilisateur.getId()).get();
        // remettre le chemin du utilisateur dans le nouvel utilisateur
        Utilisateur paramToDo = modelMapper.map(nouveauUtilisateur, Utilisateur.class);
        paramToDo.setPathImage(oldUtilisateurDo.getPathImage());
        // sauvgarder le nouvel utilisateur
        paramToDo = utilisateurDao.save(paramToDo);
        return modelMapper.map(paramToDo, UtilisateurDto.class);
    }

    @Override
    public UtilisateurDto update(UtilisateurDto utilisateur, MultipartFile uneImage) {
        Utilisateur utilisateurDoResult = modelMapper.map(utilisateur, Utilisateur.class);
        String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), utilisateurDoResult.getId());
        Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
        utilisateurDoResult.setPathImage(path.toString());
        utilisateurDoResult = utilisateurDao.save(utilisateurDoResult);
        return modelMapper.map(utilisateurDoResult, UtilisateurDto.class);
    }
}
