package fr.afpa.cda.ava.microservice.app.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@PropertySource("propertiesConfig/storage.properties")
@Configuration
public class StorageProperties {

	@Value("${storage.location-dir}")
	private String locationDir;
	@Value("${storage.produit-images}")
	private String produitImageDir;
	@Value("${storage.utilisateur-images}")
	private String utilisateurImageDir;

}
