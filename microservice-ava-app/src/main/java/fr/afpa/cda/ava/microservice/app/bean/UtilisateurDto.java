package fr.afpa.cda.ava.microservice.app.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UtilisateurDto {
	private Integer id;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	private Date dateNaissance;
	private String profil;
	private String adresse;
	private String complementAdresse;
	private String codePostal;
	private String ville;
	private String pays;
	private String fix;
	private String portable;
	@JsonIgnore
	private String pathImage;
	private String url;

}
