package fr.afpa.cda.ava.microservice.app.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.microservice.app.bean.UtilisateurDto;
import fr.afpa.cda.ava.microservice.app.services.IFileSystemStorageService;
import fr.afpa.cda.ava.microservice.app.services.IUtilisateurService;

@RestController
public class UtilisateurController {

	@Autowired
	IUtilisateurService utilisateurService;

	@Autowired
	IFileSystemStorageService fileSystemStorageService;

	@GetMapping({ "/utilisateurs" })
	public List<UtilisateurDto> getAll() {
		List<UtilisateurDto> utilisateurs = utilisateurService.findAll();
		return utilisateurs;
	}

	// DELETE /users/{idUser}
	@DeleteMapping("/utilisateurs/{id}")
	public void supprimerProduit(@PathVariable Integer id) {
		utilisateurService.deleteById(id);
	}

	// GET /utilisateurs/{idUser} ==> getById
	@GetMapping("/utilisateurs/{id}")
	public UtilisateurDto chercherProduit(@PathVariable Integer id, HttpServletRequest request) {
		return utilisateurService.findById(id);
	}

	@PostMapping("/utilisateurs")
	@ResponseStatus(value = HttpStatus.CREATED)
	public UtilisateurDto save(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
			@RequestBody UtilisateurDto utilisateur) {
		if (uneImage != null && !uneImage.isEmpty())
			return utilisateurService.save(utilisateur, uneImage);
		return utilisateurService.save(utilisateur);
	}

	@PutMapping("/utilisateurs/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public UtilisateurDto update(@RequestParam(value = "file_image", required = false) MultipartFile uneImage,
			@RequestBody UtilisateurDto utilisateur) {
		if (uneImage != null && !uneImage.isEmpty())
			return utilisateurService.update(utilisateur, uneImage);
		return utilisateurService.update(utilisateur);
	}

	@GetMapping("/login")
	public UtilisateurDto seLoger(String login, String password) {
		return utilisateurService.findByEmailAndPassword(login, password);
	}

}
