Hibernate: drop table if exists produit cascade
Hibernate: drop sequence if exists produit_id_seq
Hibernate: create sequence produit_id_seq start 1 increment 1
Hibernate: create table produit (id int4 not null, date_fabrication date, description varchar(255), nom varchar(255), path_image varchar(255), poid float4, primary key (id))
