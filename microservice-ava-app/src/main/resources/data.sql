--utilisateur
alter sequence utilisateur_id_seq restart with 1;
delete from utilisateur;
INSERT INTO utilisateur(iduser, nom, prenom, email, "password", date_naissance, profil, code_postal, complement_adresse, adresse,ville,pays, fix, portable, path_image) VALUES((select nextval('utilisateur_id_seq')), 'TOTO', 'Thomas', 'toto@gmail.com', 'toto', '31-03-1987', 'admin' , '59800', 'Appart 03', '22 rue de java','Tourcoing','France', '0345657222', '0654453888', null);
INSERT INTO utilisateur(iduser, nom, prenom, email, "password", date_naissance, profil, code_postal, complement_adresse, adresse,ville,pays, fix, portable, path_image) VALUES((select nextval('utilisateur_id_seq')), 'TITI', 'Thierry', 'titi@gmail.com', 'titi', '28-04-1982', 'client' , '59000', 'Appart 34', '54 avenue jakarta', 'Tourcoing','France','0945333654', '0654777432', null);
INSERT INTO utilisateur(iduser, nom, prenom, email, "password", date_naissance, profil, code_postal, complement_adresse, adresse,ville,pays, fix, portable, path_image) VALUES((select nextval('utilisateur_id_seq')), 'BEAUTE et BIEN-ETRE', 'BBE', 'bbe@gmail.com', 'bbe', '12-11-1990', 'prestataire' , '59000', 'Immeuble BBE', '53 rue de la beaute','Tourcoing','France', '0945876543', '0632456789', null);
INSERT INTO utilisateur(iduser, nom, prenom, email, "password", date_naissance, profil, code_postal, complement_adresse, adresse,ville,pays, fix, portable, path_image) VALUES((select nextval('utilisateur_id_seq')), 'ESTHETIQUE CENTER', 'ESTHE', 'esthe_center@gmail.com', 'esthe', '14-10-1989', 'prestataire' , '59000', 'Appart 01', '74 rue de la joie','Tourcoing','France', '0988776543', '0633456589', null);
--produit
alter sequence produit_id_seq restart with 1;
delete from produit;
INSERT INTO public.produit(id, reference, libelle, prix, path_image, prestataire) VALUES((select nextval('produit_id_seq')), 'BBESPA01', 'SPA 2h/2 personnes', 80.00, 'img/spaPrive.jpg', 'BEAUTE et BIEN-ETRE');
INSERT INTO public.produit(id, reference, libelle, prix, path_image, prestataire) VALUES((select nextval('produit_id_seq')), 'ESTHECOIF01', 'Coiffure femme - coupe carre', 25.00, 'img/coif_coupeCourt.jpg', 'ESTHETIQUE CENTER');
--calendrier
--reservation
--reservationproduit
