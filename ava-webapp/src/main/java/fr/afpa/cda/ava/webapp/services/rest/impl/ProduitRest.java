package fr.afpa.cda.ava.webapp.services.rest.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Produit;
import fr.afpa.cda.ava.webapp.properties.MicroserviceConfigProperties;
import fr.afpa.cda.ava.webapp.services.rest.IProduitRest;

@Service
public class ProduitRest implements IProduitRest {
    // mon navigateur
    WebTarget  webTarget;
    WebTarget  webTargetMultiPart;
    @Resource(name = "dateFormatApi")
    DateFormat dateFormatApi;

    @Autowired
    public ProduitRest(@Qualifier("clientJeresey") Client client, @Qualifier("clientJereseyWithMultipart") Client clientMultipart, MicroserviceConfigProperties microserviceConfigProperties) {
        this.microserviceConfigProperties = microserviceConfigProperties;
        this.webTarget = client.target(microserviceConfigProperties.getBaseUrl());
        this.webTargetMultiPart = clientMultipart.target(microserviceConfigProperties.getBaseUrl());
    }
    
    MicroserviceConfigProperties microserviceConfigProperties;

    @Override
    public List<Produit> findAll() {
        // barre d'adresse avec url jusqu'au contexte de l'application à appeler

        // mon service
        WebTarget produitWebTarget = webTarget.path(microserviceConfigProperties.getEndPointListeProduit());
        // Construction requete http
        Invocation.Builder invocationBuilder = produitWebTarget.request();

        // lancer l'appel HTTP
        return invocationBuilder.get(new GenericType<List<Produit>>() {});
    }

    @Override
    public Produit getOne(Integer id) {
        // barre d'adresse avec url jusqu'au contexte de l'application à appeler
        // mon service
        /*
         * Exemple de MessageFormat String message = "This message is for {0} in {1}."; String result = MessageFormat.format(message, "me", "the next morning"); System.out.println(result);
         */

        String endPointFindById = MessageFormat.format(microserviceConfigProperties.getEndPointProduitFindById(), id);

        WebTarget produitWebTarget = webTarget.path(endPointFindById);
        // Construction requete http
        Invocation.Builder invocationBuilder = produitWebTarget.request(MediaType.APPLICATION_JSON).header("cle", "val");
        // lancer l'appel HTTP
        Produit produit = invocationBuilder.get(Produit.class);
        return produit;
    }

    @Override
    public Produit save(Produit nouveauProduit) {
        // barre d'adresse avec url jusqu'au contexte de l'application à appeler
        // mon service
        WebTarget produitWebTarget = webTarget.path(microserviceConfigProperties.getEndPointProduitcreate());
        // Construction requete http
        Invocation.Builder invocationBuilder = produitWebTarget.request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity(nouveauProduit, MediaType.APPLICATION_JSON), Produit.class);
    }

    @Override
    public void deleteById(Integer id) {
        // barre d'adresse avec url jusqu'au contexte de l'application à appeler
        // mon service
        /*
         * Exemple de MessageFormat String message = "This message is for {0} in {1}."; String result = MessageFormat.format(message, "me", "the next morning"); System.out.println(result);
         */
        String endPointCrud = MessageFormat.format(microserviceConfigProperties.getEndPointProduitDelete(), id);
        WebTarget produitWebTarget = webTarget.path(endPointCrud);
        // Construction requete http
        Invocation.Builder invocationBuilder = produitWebTarget.request();

        // lancer l'appel HTTP
        invocationBuilder.delete();
    }

    @Override
    public Produit save(Produit nouveauProduit, MultipartFile uneImage) throws IOException {

        final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image", uneImage.getInputStream(), uneImage.getResource().getFilename());

        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        formDataMultiPart.field("id", nouveauProduit.getId() != null ? nouveauProduit.getId().toString() : "");
        formDataMultiPart.field("reference", nouveauProduit.getReference() != null ? nouveauProduit.getReference() : "");
        formDataMultiPart.field("libelle", nouveauProduit.getLibelle() != null ? nouveauProduit.getLibelle() : "");
        formDataMultiPart.field("prix", nouveauProduit.getPrix() != null ? nouveauProduit.getPrix().toString() : "");
        formDataMultiPart.field("prestataire", nouveauProduit.getLibelle() != null ? nouveauProduit.getPrestataire() : "");

        MultiPart multiPart = formDataMultiPart.bodyPart(streamDataBodyPart);
        WebTarget produitWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointProduitcreate());
        // Construction requete http
        Invocation.Builder invocationBuilder = produitWebTarget.request();

        Produit resultat = invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()), Produit.class);

        formDataMultiPart.close();

        return resultat;

    }

    @Override
    public Produit update(Produit nouveauProduit) {

        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        formDataMultiPart.field("id", nouveauProduit.getId() != null ? nouveauProduit.getId().toString() : "");
        formDataMultiPart.field("reference", nouveauProduit.getReference() != null ? nouveauProduit.getReference() : "");
        formDataMultiPart.field("libelle", nouveauProduit.getLibelle() != null ? nouveauProduit.getLibelle() : "");
        formDataMultiPart.field("prix", nouveauProduit.getPrix() != null ? nouveauProduit.getPrix().toString() : "");
        formDataMultiPart.field("prestataire", nouveauProduit.getLibelle() != null ? nouveauProduit.getPrestataire() : "");
        
        WebTarget produitWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointProduitUpdate());
        // Construction requete http
        Invocation.Builder invocationBuilder = produitWebTarget.request();

        Produit resultat = invocationBuilder.put(Entity.entity(formDataMultiPart, formDataMultiPart.getMediaType()), Produit.class);

        return resultat;
    }

    @Override
    public Produit update(Produit nouveauProduit, MultipartFile uneImage) throws IOException {

        final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image", uneImage.getInputStream(), uneImage.getResource().getFilename());

        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        formDataMultiPart.field("id", nouveauProduit.getId() != null ? nouveauProduit.getId().toString() : "");
        formDataMultiPart.field("reference", nouveauProduit.getReference() != null ? nouveauProduit.getReference() : "");
        formDataMultiPart.field("libelle", nouveauProduit.getLibelle() != null ? nouveauProduit.getLibelle() : "");
        formDataMultiPart.field("prix", nouveauProduit.getPrix() != null ? nouveauProduit.getPrix().toString() : "");
        formDataMultiPart.field("prestataire", nouveauProduit.getLibelle() != null ? nouveauProduit.getPrestataire() : "");
       
        MultiPart multiPart = formDataMultiPart.bodyPart(streamDataBodyPart);
        WebTarget produitWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointProduitUpdate());
        // Construction requete http
        Invocation.Builder invocationBuilder = produitWebTarget.request();

        Produit resultat = invocationBuilder.put(Entity.entity(multiPart, multiPart.getMediaType()), Produit.class);

        formDataMultiPart.close();

        return resultat;

    }

}
