package fr.afpa.cda.ava.webapp.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Produit;
import fr.afpa.cda.ava.webapp.services.IProduitService;
import fr.afpa.cda.ava.webapp.services.rest.IProduitRest;

/**
 * Classe implementation de l'interface IProduitService
 * 
 * @author Charline
 *
 */
@Service
public class ProduitServiceImpl implements IProduitService {
    @Autowired
    IProduitRest produitRest;

    @Override
    public List<Produit> findAll() {
        return produitRest.findAll();
    }

    @Override
    public Produit findById(Integer id) {
        return produitRest.getOne(id);// <==>
    }

    @Override
    public Produit save(Produit nouveauProduit) {
        return produitRest.save(nouveauProduit);
    }

    @Override
    public Produit update(Produit nouveauProduit) {
        return produitRest.update(nouveauProduit);
    }

    @Override
    public void deleteById(Integer id) {
        produitRest.deleteById(id);
    }

    @Override
    public Produit saveWithImage(Produit nouveauProduit, MultipartFile uneImage) throws IOException {
        return produitRest.save(nouveauProduit, uneImage);
    }

    @Override
    public Produit updateWithImage(Produit nouveauProduit, MultipartFile uneImage) throws IOException {
        return produitRest.update(nouveauProduit, uneImage);
    }

}
