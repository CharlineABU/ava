package fr.afpa.cda.ava.webapp.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.afpa.cda.ava.webapp.bean.InfosContact;
import fr.afpa.cda.ava.webapp.bean.Utilisateur;
import fr.afpa.cda.ava.webapp.services.IUtilisateurService;


/**
 * @author Charline
 *
 */
@Controller
public class UtilisateurController {
    /**
     * 
     */
    @Autowired
    private IUtilisateurService utilisateurService;

//    @GetMapping(value = { "/", "/index", "/toto" })
//    public String pageDAccueil() {
//
//        return "index_page";
//    }

    /**
     * @param model
     * @return
     */
    @GetMapping("/profil")
    public String afficherProfil(HttpSession session) {
        if (session.isNew() || ControllersUtils.getAttribut(session, "connectedUser", Utilisateur.class) == null)
            return "redirect:/profil/login";
        return "profil/profil_page";
    }

    @GetMapping("/profil/modifier")
    public String afficherModifierProfil(HttpSession session) {
        if (session.isNew() || ControllersUtils.getAttribut(session, "connectedUser", Utilisateur.class) == null)
            return "redirect:/profil/login";
        return "profil/modifier_utilisateur_page";
    }

    @PostMapping("/profil/modifier")
    public String modifierProfil(HttpSession session, @RequestParam("file_image") MultipartFile file, Utilisateur nouveauUtilisateur, InfosContact infosContact) throws IOException {
        nouveauUtilisateur.setInfosContact(infosContact);
        nouveauUtilisateur = utilisateurService.update(nouveauUtilisateur, file);
        session.setAttribute("connectedUser", nouveauUtilisateur);
        return "redirect:/profil";
    }

    @GetMapping("/profil/inscription")
    public String afficherInscriptionProfil() {
        return "profil/nouveau_utilisateur_page";
    }

    @PostMapping("/profil/inscription")
    public String inscriptionProfil(HttpSession session, @RequestParam("file_image") MultipartFile file, Utilisateur nouveauUtilisateur, InfosContact infosContact) throws IOException {
        nouveauUtilisateur.setInfosContact(infosContact);
        nouveauUtilisateur = utilisateurService.save(nouveauUtilisateur, file);
        session.setAttribute("connectedUser", nouveauUtilisateur);
        return "redirect:/profil";
    }

    @GetMapping("/profil/login")
    public String afficherSeLoger(String login, String password) throws Exception {
        //		if(true) {
        //			throw new Exception("toto");
        //		}
        return "profil/login_page";
    }

    @PostMapping("/profil/login")
    public String seLoger(HttpSession session, RedirectAttributes model, String login, String password) {
        Utilisateur connectedUser = utilisateurService.findByEmailAndPassword(login, password);

        if (connectedUser == null) {
            model.addFlashAttribute("messageErreur", "Login/mdp invalide");
            return "redirect:/profil/login";
        }

        session.setAttribute("connectedUser", connectedUser);

        return "redirect:/profil";
    }

    @GetMapping("/profil/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/profil/login";
    }

    @ExceptionHandler(Exception.class)
    public String handleNotFoudPage(Exception exc) {
        exc.printStackTrace();
        return "errors/not_found";
    }

}
