package fr.afpa.cda.ava.webapp.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.afpa.cda.ava.webapp.exceptions.StorageFileNotFoundException;
import fr.afpa.cda.ava.webapp.services.IFileService;

/**
 * Class à utiliser pour upload et download des fichier sur le serveur de l'api si besoin <br>
 * dans le cadre de ce projet les fichier sont geres directement par l'api rest microservice <br>
 * (voir les service rest UtilisateurRest.class et Produit.class pour mieux comprendre)
 * 
 * @author Charline
 */
@Controller
public class FileUploadController {
    @Autowired
    private IFileService storageService;

    @GetMapping("/upload")
    public String listUploadedFiles(Model model) throws IOException {
        List<String> spathStream = storageService.loadAll()
                        .map(path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString()).build().toString()).collect(Collectors.toList());

        model.addAttribute("files", spathStream);

        return "upload-download-file/uploadForm";
    }

    @GetMapping("/upload/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/images/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveImage(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename, "produits/imgs");
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/user/image/{id}/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveProfil(@PathVariable Integer id, @PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename, "profils/" + id + "/imgs");
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("name") String nom, RedirectAttributes redirectAttributes) {

        storageService.store(file);

        redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/upload/";
    }

    @ExceptionHandler(value = { StorageFileNotFoundException.class })
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}
