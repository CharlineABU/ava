package fr.afpa.cda.ava.webapp.bean;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Utilisateur {
    private Integer      id;
    private String       nom;
    private String       prenom;
    private String       email;
    private String       password;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date         dateNaissance;
    private String       profil;
    private InfosContact infosContact;
    private String       url;

}
