/**
 * 
 */
package fr.afpa.cda.ava.webapp.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Classe ProduitDto
 * 
 * @author Charline
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Produit {

    private Integer id;
    private String  reference;
    private String  libelle;
    private Double  prix;
    private String  url;
    private String  prestataire;

}
