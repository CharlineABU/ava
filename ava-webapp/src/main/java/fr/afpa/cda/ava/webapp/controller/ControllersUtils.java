package fr.afpa.cda.ava.webapp.controller;

import javax.servlet.http.HttpSession;

public class ControllersUtils {

	public static <T> T getAttribut(HttpSession session, String name, Class<T> clazz) {
		return clazz.cast(session.getAttribute(name));
	}

}
