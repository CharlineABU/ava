package fr.afpa.cda.ava.webapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties({  })
public class AvaWebappApplication {

    public static void main(final String[] args) {
        SpringApplication.run(AvaWebappApplication.class, args);
    }

    @Bean(value = "clientJeresey")
    Client client() {
        return ClientBuilder.newClient();
    }

    @Bean(value = "clientJereseyWithMultipart")
    Client clientWithMultipart() {
        return ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
    }

    @Bean(value = "dateFormatApi")
    DateFormat dateFormatApi() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }

}
