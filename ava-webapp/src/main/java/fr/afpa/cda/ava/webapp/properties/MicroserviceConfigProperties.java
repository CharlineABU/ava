package fr.afpa.cda.ava.webapp.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@PropertySource("propertiesConfig/microservice-conf.properties")
@Configuration
public class MicroserviceConfigProperties {
	@Value("${microservice-base-url}")
	private String baseUrl;
	
	@Value("${microservice-end-point.liste-produit}")
	private String endPointListeProduit;
	
	@Value("${microservice-end-point.produit.create}")
	private String endPointProduitcreate;
	
	@Value("${microservice-end-point.produit.update}")
	private String endPointProduitUpdate;
	
	@Value("${microservice-end-point.produit.delete}")
	private String endPointProduitDelete;
	
	@Value("${microservice-end-point.produit.findById}")
	private String endPointProduitFindById;
	
	@Value("${microservice-end-point.liste-utilisateurs}")
	private String endPointListeUtilisateurs;
	
	@Value("${microservice-end-point.utilisateur.seLoger}")
	private String endPointUtilisateurSeLoger;
	
	@Value("${microservice-end-point.utilisateur.create}")
	private String endPointUtilisateurCreate;
	
	@Value("${microservice-end-point.utilisateur.update}")
	private String endPointUtilisateurUpdate;
	
	@Value("${microservice-end-point.utilisateur.delete}")
	private String endPointUtilisateurDelete;
	
	@Value("${microservice-end-point.utilisateur.findById}")
	private String endPointUtilisateurFindById;
}
