package fr.afpa.cda.ava.webapp.services.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Utilisateur;

public interface IUtilisateurRest {

    public List<Utilisateur> findAll();

    public Utilisateur getOne(Integer id);

    public void deleteById(Integer id);

    public Utilisateur save(Utilisateur nouveauUtilisateur, MultipartFile fileImage) throws IOException;

    public Utilisateur update(Utilisateur nouveauUtilisateur, MultipartFile fileImage) throws IOException;

    public Utilisateur seLoger(String login, String password);

}
