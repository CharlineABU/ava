package fr.afpa.cda.ava.webapp.services.rest.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Utilisateur;
import fr.afpa.cda.ava.webapp.properties.MicroserviceConfigProperties;
import fr.afpa.cda.ava.webapp.services.rest.IUtilisateurRest;

@Service
public class UtilisateurRest implements IUtilisateurRest {

    // mon navigateur
    WebTarget  webTarget;
    WebTarget  webTargetMultiPart;
    @Resource(name = "dateFormatApi")
    DateFormat dateFormatApi;

    @Autowired
    public UtilisateurRest(@Qualifier("clientJeresey") Client client, @Qualifier("clientJereseyWithMultipart") Client clientMultipart, MicroserviceConfigProperties microserviceConfigProperties) {
        this.microserviceConfigProperties = microserviceConfigProperties;
        this.webTarget = client.target(microserviceConfigProperties.getBaseUrl());
        this.webTargetMultiPart = clientMultipart.target(microserviceConfigProperties.getBaseUrl());
    }

    MicroserviceConfigProperties microserviceConfigProperties;

    @Override
    public List<Utilisateur> findAll() {
        // barre d'adresse avec url jusqu'au contexte de l'application à appeler

        // mon service
        WebTarget utilisateurWebTarget = webTarget.path(microserviceConfigProperties.getEndPointListeUtilisateurs());
        // Construction requete http
        Invocation.Builder invocationBuilder = utilisateurWebTarget.request();

        // lancer l'appel HTTP
        return invocationBuilder.get(new GenericType<List<Utilisateur>>() {});
    }

    @Override
    public Utilisateur getOne(Integer id) {
        // barre d'adresse avec url jusqu'au contexte de l'application à appeler
        // mon service
        /*
         * Exemple de MessageFormat String message = "This message is for {0} in {1}."; String result = MessageFormat.format(message, "me", "the next morning"); System.out.println(result);
         */

        String endPointFindById = MessageFormat.format(microserviceConfigProperties.getEndPointUtilisateurFindById(), id);

        WebTarget utilisateurWebTarget = webTarget.path(endPointFindById);
        // Construction requete http
        Invocation.Builder invocationBuilder = utilisateurWebTarget.request(MediaType.APPLICATION_JSON).header("cle", "val");
        // lancer l'appel HTTP
        Utilisateur produit = invocationBuilder.get(Utilisateur.class);
        return produit;
    }

    @Override
    public void deleteById(Integer id) {
        /*
         * Exemple de MessageFormat String message = "This message is for {0} in {1}."; String result = MessageFormat.format(message, "me", "the next morning"); System.out.println(result);
         */
        String endPointCrud = MessageFormat.format(microserviceConfigProperties.getEndPointUtilisateurDelete(), id);
        WebTarget utilisateurWebTarget = webTarget.path(endPointCrud);
        // Construction requete http
        Invocation.Builder invocationBuilder = utilisateurWebTarget.request();

        // lancer l'appel HTTP
        invocationBuilder.delete();
    }

    @Override
    public Utilisateur update(Utilisateur nouveauUtilisateur, MultipartFile fileImage) throws IOException {

        FormDataMultiPart formDataMultiPart = mapUtilisateurDtoToParamRequest(nouveauUtilisateur);

        if (fileImage != null && !fileImage.isEmpty()) {
            final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image", fileImage.getInputStream(), fileImage.getResource().getFilename());
            formDataMultiPart.bodyPart(streamDataBodyPart);
        }

        MultiPart multiPart = formDataMultiPart;
        WebTarget utilisateurWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointUtilisateurUpdate());
        // Construction requete http
        Invocation.Builder invocationBuilder = utilisateurWebTarget.request();
        return invocationBuilder.put(Entity.entity(multiPart, multiPart.getMediaType()), Utilisateur.class);
    }

    @Override
    public Utilisateur save(Utilisateur nouveauUtilisateur, MultipartFile fileImage) throws IOException {

        FormDataMultiPart formDataMultiPart = mapUtilisateurDtoToParamRequest(nouveauUtilisateur);

        if (fileImage != null && !fileImage.isEmpty()) {
            final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image", fileImage.getInputStream(), fileImage.getResource().getFilename());
            formDataMultiPart.bodyPart(streamDataBodyPart);
        }

        MultiPart multiPart = formDataMultiPart;
        WebTarget utilisateurWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointUtilisateurCreate());
        // Construction requete http
        Invocation.Builder invocationBuilder = utilisateurWebTarget.request();
        return invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()), Utilisateur.class);
    }

    @Override
    public Utilisateur seLoger(String login, String password) {
        String endPointSeLoger = microserviceConfigProperties.getEndPointUtilisateurSeLoger();

        WebTarget utilisateurWebTarget = webTarget.path(endPointSeLoger).queryParam("login", login).queryParam("password", password);
        // Construction requete http
        Invocation.Builder invocationBuilder = utilisateurWebTarget.request();
        return invocationBuilder.get(Utilisateur.class);
    }

    private FormDataMultiPart mapUtilisateurDtoToParamRequest(Utilisateur nouveauUtilisateur) {
        // utilisateur
        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        formDataMultiPart.field("id", nouveauUtilisateur.getId() != null ? nouveauUtilisateur.getId().toString() : "");
        formDataMultiPart.field("nom", nouveauUtilisateur.getNom() != null ? nouveauUtilisateur.getNom() : "");
        formDataMultiPart.field("prenom", nouveauUtilisateur.getPrenom() != null ? nouveauUtilisateur.getPrenom() : "");
        formDataMultiPart.field("email", nouveauUtilisateur.getEmail() != null ? nouveauUtilisateur.getEmail() : "");
        formDataMultiPart.field("password", nouveauUtilisateur.getPassword() != null ? nouveauUtilisateur.getPassword() : "");
        formDataMultiPart.field("dateNaissance", nouveauUtilisateur.getDateNaissance() != null ? dateFormatApi.format(nouveauUtilisateur.getDateNaissance()) : "");
        formDataMultiPart.field("profil", nouveauUtilisateur.getProfil() != null ? nouveauUtilisateur.getProfil() : "");
        
        // infosContact
        if (nouveauUtilisateur.getInfosContact() != null) {
            formDataMultiPart.field("ligne1", nouveauUtilisateur.getInfosContact().getLigne1() != null ? nouveauUtilisateur.getInfosContact().getLigne1() : "");
            formDataMultiPart.field("complement", nouveauUtilisateur.getInfosContact().getComplement() != null ? nouveauUtilisateur.getInfosContact().getComplement() : "");
            formDataMultiPart.field("codePostal", nouveauUtilisateur.getInfosContact().getCodePostal() != null ? nouveauUtilisateur.getInfosContact().getCodePostal() : "");
            formDataMultiPart.field("fix", nouveauUtilisateur.getInfosContact().getFix() != null ? nouveauUtilisateur.getInfosContact().getFix() : "");
            formDataMultiPart.field("portable", nouveauUtilisateur.getInfosContact().getPortable() != null ? nouveauUtilisateur.getInfosContact().getPortable() : "");
        }
        return formDataMultiPart;
    }

}
