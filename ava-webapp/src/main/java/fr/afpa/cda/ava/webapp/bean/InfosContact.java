package fr.afpa.cda.ava.webapp.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InfosContact {

    private String ligne1;
    private String complement;
    private String codePostal;
    private String fix;
    private String portable;

}
