package fr.afpa.cda.ava.webapp.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Produit;

/**
 * Interface ProduitService
 * 
 * @author Charline
 *
 */
public interface IProduitService {

    /**
     * Permet de recuperer la liste des produitDto
     * 
     * @return la liste des produits récupérée sinon liste vide
     */
    public List<Produit> findAll();

    /**
     * Permet de chercher un produitDto par son Id
     * 
     * @param id du produitDto à chercher
     * @return produitDto trouvé sinon null
     */
    public Produit findById(final Integer id);

    /**
     * Permet de créer un nouveau produit
     * 
     * @param nouveauProduit à créer
     * @return le produit crée sinon null
     */
    public Produit save(final Produit nouveauProduit);

    /**
     * Permet de mettre à jour un produit
     * 
     * @param nouveauProduit à mettre à jour
     * @return produit mis à jour sinon null
     */
    public Produit update(final Produit nouveauProduit);

    /**
     * Permet de supprimer un produit
     * 
     * @param id
     */
    public void deleteById(final Integer id);

    /**
     * Permet de créer un nouveau produit avec une image
     * 
     * @param nouveauProduit
     * @param uneImage
     * @return
     * @throws IOException
     */
    public Produit saveWithImage(Produit nouveauProduit, MultipartFile uneImage) throws IOException;

    /**
     * Permet de mettre à jour un produit avec image
     * 
     * @param nouveauProduit
     * @param uneImage
     * @return
     * @throws IOException
     */
    public Produit updateWithImage(Produit nouveauProduit, MultipartFile uneImage) throws IOException;
}
