package fr.afpa.cda.ava.webapp.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Produit;
import fr.afpa.cda.ava.webapp.services.IProduitService;

/**
 * @author Charline
 *
 */
@Controller
public class ProduitController {

    @Autowired
    private IProduitService produitService;

    //** Liste des Produit

    // http://localhost:9090/produits/liste_produits
    /**
     * affiche la liste des produits
     * 
     * @param model
     * @return
     */
    @GetMapping(value = { "liste_produits", "/" })
    public String afficherListeProduits(final Model model) {
        
        model.addAttribute("produits", produitService.findAll());
        return "produit/liste_produits_page";
    }

    //** Detail d'un Produit

    // http://localhost:9090/produits/detail/1
    /**
     * Permet d'afficher le detail d'un produit
     * 
     * @param model
     * @param idProduit
     * @return
     */
    @GetMapping("/produits/detail/{id}")
    public String afficherDetailProduitAvecVariablePath(final Model model, final @PathVariable("id") Integer idProduit) {
        Produit produit = produitService.findById(idProduit);
        model.addAttribute("produit", produit);
        return "produit/detail_produit_page";
    }

    //** Creer un Produit */

    // Get http://localhost:9090/produits/new
    /**
     * affiche page de creation d'un nouveau produit
     * 
     * @return
     */
    @GetMapping("/produits/new")
    public String afficherCreerProduit() {
        return "produit/creer_produit_page";
    }

    // POST http://localhost:9090/produits/new
    /**
     * Permet de creer un nouveau produit avec une image
     * 
     * @param uneImage
     * @param nouveauProduit
     * @return redirection vers l'ecran liste des produit
     */
    @PostMapping("/produits/new")
    public String creerProduitAvecImage(final @RequestParam("file_image") MultipartFile uneImage, final Produit nouveauProduit) throws IOException {

        produitService.saveWithImage(nouveauProduit, uneImage);

        return "redirect:/liste_produits";

    }

    //** Update un Produit

    // Get http://localhost:9090/produits/update/{id}
    /**
     * affiche la page de mise à jour d'un produit
     * 
     * @return
     */
    @GetMapping("/produits/update/{id}")
    public String afficherUpdateProduit(final @PathVariable("id") Integer id, final Model model) {
        model.addAttribute("produit", produitService.findById(id));
        return "produit/update_produit_page";
    }

    // POST http://localhost:9090/produits/update
    /**
     * Permet de mettre à jour un produit
     * 
     * @param uneImage
     * @param nouveauProduit
     * @return redirection vers l'ecran liste des produit
     */
    @PostMapping("/produits/update")
    public String updateProduit(final @RequestParam("file_image") MultipartFile uneImage, final Produit nouveauProduit) throws IOException {

        // l'utilisateur demande de changer d'image
        if (uneImage == null || uneImage.isEmpty()) {
            produitService.update(nouveauProduit);
        } else {
            produitService.updateWithImage(nouveauProduit, uneImage);
        }

        return "redirect:/liste_produits";

    }

    //** Delete un Produit

    // http://localhost:9090/produits/delete/1
    /**
     * Permet de supprimer un produit
     * 
     * @param idProduit du produit à supprimer
     * @return redirection vers l'ecran liste des produit
     */
    @GetMapping("/produits/delete/{id}")
    public String delete(@PathVariable("id") Integer idProduit) {
        produitService.deleteById(idProduit);
        return "redirect:/liste_produits";
    }

}
