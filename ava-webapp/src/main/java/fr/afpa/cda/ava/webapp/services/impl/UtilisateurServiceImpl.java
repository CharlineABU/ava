package fr.afpa.cda.ava.webapp.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Utilisateur;
import fr.afpa.cda.ava.webapp.services.IUtilisateurService;
import fr.afpa.cda.ava.webapp.services.rest.IUtilisateurRest;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService {

    @Autowired
    IUtilisateurRest utilisateurRest;

    @Override
    public List<Utilisateur> findAll() {
        return utilisateurRest.findAll();
    }

    @Override
    public Utilisateur findById(Integer id) {
        return utilisateurRest.getOne(id);
        // return utilisateurDao.findById(id).get();
    }

    @Override
    public Utilisateur save(Utilisateur nouveauUtilisateur, MultipartFile file) throws IOException {

        return utilisateurRest.save(nouveauUtilisateur, file);
    }

    @Override
    public Utilisateur update(Utilisateur nouveauUtilisateur, MultipartFile file) throws IOException {

        return utilisateurRest.update(nouveauUtilisateur, file);
    }

    @Override
    public void deleteById(Integer id) {
        utilisateurRest.deleteById(id);

    }

    @Override
    public Utilisateur findByEmailAndPassword(String login, String password) {
        Utilisateur u = utilisateurRest.seLoger(login, password);

        return u;
    }

}
