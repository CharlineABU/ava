package fr.afpa.cda.ava.webapp.services.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Produit;

public interface IProduitRest {

    public List<Produit> findAll();

    public Produit getOne(Integer id);

    public Produit save(Produit nouveauProduit);

    public void deleteById(Integer id);

    public Produit update(Produit nouveauProduit);

    public Produit save(Produit nouveauProduit, MultipartFile uneImage) throws IOException;

    public Produit update(Produit nouveauProduit, MultipartFile uneImage) throws IOException;

}
