package fr.afpa.cda.ava.webapp.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ava.webapp.bean.Utilisateur;

/**
 * @author Charline
 *
 */
public interface IUtilisateurService {
	public List<Utilisateur> findAll();

	public Utilisateur findById(Integer id);

	public void deleteById(Integer id);

	public Utilisateur findByEmailAndPassword(String login, String password);

	Utilisateur save(Utilisateur nouveauUtilisateur, MultipartFile file) throws IOException;

	Utilisateur update(Utilisateur nouveauUtilisateur, MultipartFile file) throws IOException;
}
